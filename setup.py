from setuptools import setup

setup(name='hue-lights-service',
      version='0.1',
      description='Plaza service to control philips hue lights.',
      author='adri1177',
      author_email='adrianfernandezmartin@gmail.com',
      license='Apache License 2.0',
      packages=['hue_lights_service'],
      scripts=['bin/hue-lights-service'],
      include_package_data=True,
      install_requires=[
          'xdg',
      ],
      zip_safe=False)
