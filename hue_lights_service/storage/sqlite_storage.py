import os
import sqlite3
from xdg import XDG_DATA_HOME

DB_PATH_ENV = 'PLAZA_PHILIPS_HUE_BRIDGE_DB_PATH'
if os.getenv(DB_PATH_ENV, None) is None:
    DATA_DIRECTORY = os.path.join(XDG_DATA_HOME, "plaza", "bridges", "philips-hue")
    DEFAULT_PATH = os.path.join(DATA_DIRECTORY, 'db.sqlite3')
else:
    DEFAULT_PATH = os.getenv(DB_PATH_ENV)
    DATA_DIRECTORY = os.path.dirname(DEFAULT_PATH)


class DBContext:
    def __init__(self, db, close_on_exit=True):
        self.db = db
        self.close_on_exit = close_on_exit

    def __enter__(self):
        return self.db

    def __exit__(self, exc_type, exc_value, tb):
        if self.close_on_exit:
            self.db.close()


class SqliteStorage:
    def __init__(self, path, multithread=True):
        self.path = path
        self.db = None
        self.multithread = multithread
        self._create_db_if_not_exists()

    def _open_db(self):
        if not self.multithread:
            if self.db is None:
                self.db = sqlite3.connect(self.path)
                self.db.execute("PRAGMA foreign_keys = ON;")
            db = self.db
        else:
            db = sqlite3.connect(self.path)
            db.execute("PRAGMA foreign_keys = ON;")

        return DBContext(db, close_on_exit=not self.multithread)

    def _create_db_if_not_exists(self):
        os.makedirs(DATA_DIRECTORY, exist_ok=True)
        with self._open_db() as db:
            c = db.cursor()
            c.execute(
                """
            CREATE TABLE IF NOT EXISTS PHILIPS_HUE_USER_REGISTRATION (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                philips_token VARCHAR(256) UNIQUE,
                philips_ip VARCHAR(256)
            );
            """
            )

            c.execute(
                """
            CREATE TABLE IF NOT EXISTS PLAZA_USERS (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                plaza_user_id VARCHAR(36) UNIQUE
            );
            """
            )

            c.execute(
                """
            CREATE TABLE IF NOT EXISTS PLAZA_USERS_IN_PHILIPS (
                plaza_id INTEGER,
                philips_id INTEGER,
                UNIQUE(plaza_id, philips_id),
                FOREIGN KEY(plaza_id) REFERENCES PLAZA_USERS(id),
                FOREIGN KEY(philips_id) REFERENCES PHILIPS_HUE_USER_REGISTRATION(id)
            );
            """
            )
            db.commit()
            c.close()

    def is_philips_hue_user_registered(self, user_id):
        with self._open_db() as db:
            c = db.cursor()
            c.execute(
                """
            SELECT count(1)
            FROM PHILIPS_HUE_USER_REGISTRATION
            WHERE philips_token=?
            ;
            """,
                (user_id,),
            )
            result = c.fetchone()[0]

            c.close()

            return result > 0

    '''def get_plaza_user_from_philips_hue(self, user_id):
        ## Warning: Untested!
        with self._open_db() as db:
            c = db.cursor()
            c.execute(
                """
            SELECT plaza_user_id
            FROM PLAZA_USERS as p
            JOIN PLAZA_USERS_IN_PHILIPS as plaza_in_philips
            ON plaza_in_philips.plaza_id = p.id
            JOIN PHILIPS_HUE_USER_REGISTRATION as m
            ON plaza_in_philips.philips_id = m.id
            WHERE m.gitlab_user_id=?
            ;
            """,
                (user_id,),
            )
            results = c.fetchall()

            c.close()
            assert 0 <= len(results) <= 1
            if len(results) == 0:
                raise Exception("User (philips_hue:{}) not found".format(user_id))
            return results[0][0]'''

    def _get_or_add_philips_hue_user(self, cursor, philips_hue_data):

        philips_token = philips_hue_data["token"]
        philips_ip = philips_hue_data["ip"]

        cursor.execute(
            """
        SELECT id
        FROM PHILIPS_HUE_USER_REGISTRATION
        WHERE philips_token=?
        ;
        """,
            (philips_token,),
        )

        results = cursor.fetchall()
        if len(results) == 0:  # New user
            cursor.execute(
                """
            INSERT INTO PHILIPS_HUE_USER_REGISTRATION (
                philips_token,philips_ip
            ) VALUES(?,?);
            """,
                (philips_token,philips_ip),
            )
            return cursor.lastrowid
        elif len(results) == 1:  # Existing user
            return results[0][0]
        else:  # This shouldn't happen
            raise Exception(
                "Integrity error, query by UNIQUE returned multiple values: {}".format(
                    cursor.rowcount
                )
            )

    def _get_or_add_plaza_user(self, cursor, plaza_user):
        cursor.execute(
            """
        SELECT id
        FROM PLAZA_USERS
        WHERE plaza_user_id=?
        ;
        """,
            (plaza_user,),
        )

        results = cursor.fetchall()
        if len(results) == 0:  # New user
            cursor.execute(
                """
            INSERT INTO PLAZA_USERS (plaza_user_id) VALUES(?);
            """,
                (plaza_user,),
            )
            return cursor.lastrowid
        elif len(results) == 1:  # Existing user
            return results[0][0]
        else:  # This shouldn't happen
            raise Exception(
                "Integrity error, query by UNIQUE returned multiple values: {}".format(
                    cursor.rowcount
                )
            )

    def register_user(self, philips_hue_data, plaza_user):
        with self._open_db() as db:
            c = db.cursor()
            philips_id = self._get_or_add_philips_hue_user(c, philips_hue_data)
            plaza_id = self._get_or_add_plaza_user(c, plaza_user)
            c.execute(
                """
            INSERT OR REPLACE INTO 
            PLAZA_USERS_IN_PHILIPS (plaza_id, philips_id)
            VALUES (?, ?)
            """,
                (plaza_id, philips_id),
            )
            c.close()
            db.commit()

    def get_philips_users(self, plaza_user):
        with self._open_db() as db:
            c = db.cursor()
            plaza_id = self._get_or_add_plaza_user(c, plaza_user)
            c.execute(
                """
            SELECT philips_u.philips_token,philips_u.philips_ip
            FROM PHILIPS_HUE_USER_REGISTRATION philips_u
            JOIN PLAZA_USERS_IN_PHILIPS plaza_in_philips
            ON philips_u.id=plaza_in_philips.philips_id
            WHERE plaza_in_philips.plaza_id=?
            ;
            """,
                (plaza_id,),
            )
            results = c.fetchall()
            c.close()
            return [
                dict(zip(["philips_token","philips_ip"], row))
                for row in results
            ]


def get_default():
    return SqliteStorage(DEFAULT_PATH)
