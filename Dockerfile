FROM python:3.7-alpine

RUN apk update && apk upgrade && \
	apk add --no-cache \
		git \
		openssh \
		gcc \
		musl-dev \
		python3-dev \
		libffi-dev \
		openssl-dev

COPY bin ./bin/
COPY hue_lights_service ./hue_lights_service/
COPY requirements.txt .
COPY setup.py .
RUN python3 -m pip install -r requirements.txt
RUN python3 setup.py develop
ENV PLAZA_BRIDGE_ENDPOINT ""
CMD hue-lights-service
